(function() {

	// private stuff

	// Model
	var category = {
		idCat : null,
		nameCat : null,

		idNiveau : null,
		nameNiveau : null,

		toHTML : function() {
			var docfrag = document.createDocumentFragment();

			var div = document.createElement("div");
			div.setAttribute("class", "category");
			div.innerHTML = this.nameCat + "<br/>" + this.nameNiveau;

			var close = document.createElement("a");
			close.setAttribute("href", "#");
			close.setAttribute("class", "close-button");
			close.innerHTML = "x";

			$(close).click(this, function(cat) {
				if (adnetest.removeCategory(cat.data)) {
					div.parentElement.removeChild(div);
				}
			});

			div.appendChild(close);
			docfrag.appendChild(div);
			return docfrag;
		}
	};

	var question = {
		title : null,
		choice : null,
		answers : [],
		category : null,

		toHTML : function(cpt) {

			if (typeof cpt == "undefined") {
				return null;
			}
			var docfrag = document.createDocumentFragment();

			var h3 = document.createElement("li");
			h3.innerHTML = this.title;

			var ul = document.createElement("ul");

			for ( var i = 0; i < this.answers.length; i++) {
				var li = document.createElement("li");
				var input = document.createElement("input");

				if (this.choice != "multiple") {
					input.setAttribute("type", "radio");
				} else {
					input.setAttribute("type", "checkbox");
				}
				input.setAttribute("id", "id" + i);
				input.setAttribute("name", "name" + cpt);
				input.setAttribute("value", "value" + i);

				var label = document.createElement("label");
				label.setAttribute("for", "id" + i);
				label.innerHTML = this.answers[i].title;
				li.appendChild(input);
				li.appendChild(label);
				ul.appendChild(li);

				$(input).click({
					"input" : input,
					"cpt" : i,
					"category" : this.category,
					"question" : this
				}, function(event) {
					if (event.data.input.checked) {
						var answer = jQuery.extend(true, {}, adnetest.Answer);

						answer.category = event.data.category;
						answer.question = event.data.question;
						answer.indexAnswer = event.data.cpt;

						adnetest.addOrUpdateAnswer(answer);
					} else {
						adnetest.removeAnswer(answer);
					}
				});
			}

			docfrag.appendChild(h3);

			docfrag.appendChild(document.createElement("li").appendChild(ul));
			return docfrag;
		}

	};

	var answer = {

		category : null,
		question : null,
		indexAnswer : null

	};

	var displayQuestions = function(cat) {
		$.ajax({
			url : adnetest.projectName + '/questions',
			type : "POST",
			data : {
				catid : cat.idCat,
				levelid : cat.idNiveau
			},
			context : this
		}).done(function(data) {
			for ( var i = 0; i < data.length; i++) {

				var question = jQuery.extend(true, {}, adnetest.Question);
				question.title = data[i].title;
				question.choice = data[i].choice;
				question.answers = data[i].answers.answer;

				var category = jQuery.extend(true, {}, adnetest.Category);
				category.idCat = cat.idCat;
				category.idNiveau = cat.idNiveau;
				question.category = category;

				var list = document.createElement("ol");
				list.appendChild(question.toHTML(i));
				$("#questions").append(list);
			}
		});
	};

	// public stuff
	adnetest.Category = category;
	adnetest.Question = question;
	adnetest.Answer = answer;
	adnetest.displayQuestions = displayQuestions;
})();
