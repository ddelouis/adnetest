/* Author David RALUY

 */

//Namespaces
adnetest = {};
adnetest.projectName = "/adnetest";
(function() {

	/* Private stuff */
	var selectedCategories = [];
	var selectedAnswers = [];

	var addOrUpdateAnswer = function(answer) {
		var result = $.grep(selectedAnswers, function(e) {
			return (e.question.title == answer.question.title && e.question.choice == answer.question.choice
					&& e.category.idNiveau == answer.category.idNiveau
					&& e.category.idCat == answer.category.idCat);
		});
		if (result.length == 0) {
			selectedAnswers.push(answer);
			console.log("Added " + answer + " to list of answers selected");
			return true;
		} else {
			// update the answer
			for ( var i = 0; i < selectedAnswers.length; i++) {
				if (selectedAnswers[i].question.title == answer.question.title
						&& selectedAnswers[i].category.idCat == answer.category.idCat
						&& selectedAnswers[i].category.idNiveau == answer.category.idNiveau) {
					selectedAnswers[i] = answer;
					return true;
				}
			}
		}
		return false;
	};

	var removeAnswer = function(answer) {
		var originalLength = selectedAnswers.length;
		selectedAnswers = $.grep(selectedAnswers, function(e) {
			return (e.title == answer.title && e.choice == answer.choice);
		});
		console.log("Removed " + answer + " to list of answers selected");
		return selectedAnswers.length < originalLength;
	};

	var addCategory = function(cat) {
		var result = $.grep(selectedCategories, function(e) {
			return (e.idCat == cat.idCat && e.idNiveau == cat.idNiveau);
		});
		if (result.length == 0) {
			selectedCategories.push(cat);
			console.log("Added " + cat + " to list of categories selected");
			return true;
		}
		return false;
	};

	var removeCategory = function(cat) {
		var originalLength = selectedCategories.length;
		selectedCategories = $.grep(selectedCategories, function(e) {
			return (e.idCat != cat.idCat || e.idNiveau != cat.idNiveau);
		});
		console.log("Removed " + cat + " to list of categories selected");
		return selectedCategories.length < originalLength;
	};

	var loadCombobox = function(id, url, args, callback) {

		var reqtype = typeof args != 'undefined' ? "POST" : "GET";
		var reqdata = typeof args != 'undefined' ? args : {};
		$.ajax({
			url : adnetest.projectName + url,
			type : reqtype,
			data : reqdata
		}).done(
				function(data) {

					// this list is required to have id and name attributes

					var options = "";
					for ( var i = 0; i < data.length; i++) {
						options += '<option value="' + data[i].id + '">'
								+ data[i].name + '</option>';
					}
					$("select#" + id).html(options);

					if (typeof callback != 'undefined') {
						callback();
					}
				});
	};

	var displayContent = function(url, args) {
		var reqtype = typeof args != 'undefined' ? "POST" : "GET";
		var reqdata = typeof args != 'undefined' ? args : {};
		$.ajax({
			url : adnetest.projectName + url,
			type : reqtype,
			data: reqdata
		}).done(function(data) {
			$("#content").html(data);
		});
	};

	/* Public stuff */
	adnetest.displayContent = displayContent;
	adnetest.loadCombobox = loadCombobox;
	adnetest.addCategory = addCategory;
	adnetest.removeCategory = removeCategory;
	adnetest.getCategories = function() {
		return selectedCategories;
	};
	adnetest.addOrUpdateAnswer = addOrUpdateAnswer;
	adnetest.removeAnswer = removeAnswer;
	adnetest.getAnswers = function() {
		return selectedAnswers;
	};

})();

$(function() {
	adnetest.displayContent('/questionSelection');
});
