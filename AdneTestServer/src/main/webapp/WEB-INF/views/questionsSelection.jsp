<script>
	(function() {

		var callback = function(){loadLevels();};
		
		var loadLevels = function() {
			adnetest.loadCombobox("level_list", "/levels", {
				"catid" : $("#category_list").val()
			});
		};
		
		adnetest.loadCombobox("category_list", "/categories",null ,callback);
		
		$("#category_list").change(loadLevels);

		$("#add_category").click(function() {
			var cat = jQuery.extend(true, {}, adnetest.Category);

			cat.idCat = $("select[id=category_list] option:selected").val();
			cat.nameCat = $("select[id=category_list] option:selected").text();
			cat.idNiveau = $("select[id=level_list] option:selected").val();
			cat.nameNiveau = $("select[id=level_list] option:selected").text();

			if (cat.nameNiveau != "") {
				if (adnetest.addCategory(cat)) {
					$("#selected-categories").append(cat.toHTML());
				}
			}
		});

		$("#launch_test").click(function() {
			if (adnetest.getCategories().length > 0) {
				adnetest.displayContent("/test");
			}
		});

	})();
</script>

<form id="selection_test" action="#">
	<fieldset>
		<legend>S�lection des cat�gories:</legend>
		<div id="selected-categories"></div>
		<label class="selector" for="category_list">Cat�gorie<select
			id="category_list">
		</select></label> <label class="selector" for="level_list">Niveau<select
			id="level_list">
		</select></label>
		<div class="selector">
			<input id="add_category" type="button" value="Ajouter" />
		</div>
	</fieldset>

</form>
<input id="launch_test" type="button" value="Lancer le test >>" />