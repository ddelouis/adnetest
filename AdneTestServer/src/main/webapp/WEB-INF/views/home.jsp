<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="false" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>


<html class="no-js" lang="en">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>ADNEOM - Tests techniques</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Mobile viewport optimized: j.mp/bplateviewport -->
<meta name="viewport" content="width=device-width,initial-scale=1">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

<!-- CSS: implied media=all -->
<!-- CSS concatenated and minified via ant build script-->

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/style.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/adnetest.css">

<!-- end CSS-->

<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

<!-- All JavaScript at the bottom, except for Modernizr / Respond.
		Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
		For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
<script
	src="<%=request.getContextPath()%>/resources/js/libs/modernizr-2.0.6.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/libs/jquery-1.6.2.js"></script>
<!-- scripts concatenated and minified via ant build script-->
<script src="<%=request.getContextPath()%>/resources/js/plugins.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/script.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/questions.js"></script>
</head>

<body>

	<header class>
		<div class="menu-fixed">
			<nav>
				<div class="header_menu">
					<div class="box-logo">
						<a class="logo" href="#" title="Adneom">Adneom logo</a>
					</div>
					<h2>
						<a href="#" title="Adneom">Adnetest</a>
					</h2>
				</div>
			</nav>
		</div>
	</header>


	<div id="content"></div>


</body>
</html>
