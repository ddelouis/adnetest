package com.adneom.adnetest.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.adneom.adnetest.Question;
import com.adneom.adnetest.DAO.IQuestionsDAO;
import com.adneom.adnetest.DAO.QuestionsDAOImpl;
import com.adneom.adnetest.models.Answer;
import com.adneom.adnetest.models.Answers;

@Controller
public class TestController {

	private static final Logger logger = LoggerFactory
			.getLogger(TestController.class);

	private IQuestionsDAO dao = new QuestionsDAOImpl();

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		return "test";
	}

	@RequestMapping(value = "/checkresults", method = RequestMethod.POST)
	public ModelAndView checkresults(Locale locale, Model model,
			@RequestParam(value = "answers") String answers) {
		Answers ans = new Answers();
		ans.fromJSON(answers);

		int score = 0;
		float nbQuestions = 0;
		float nbReponsesCorrectes = 0;
		for (Answer answer : ans.getAnswers()) {
			List<Question> questions = dao.getQuestions(answer.getCat(),
					answer.getLevel());

			for (Question q : questions) {

				if (q.getTitle().equals(answer.getQuestion().getTitle())) {

					float value =  ((com.adneom.adnetest.Answer) q.getAnswers()
							.getAnswer().toArray()[answer.getIndex()]).getValue();
					score += value;
					
					if (value >0)
					{
						nbReponsesCorrectes++;
					}
					nbQuestions ++;
				}
				
			}
		}
		float scorePct = (nbReponsesCorrectes/nbQuestions)*100;

		ModelAndView mav = new ModelAndView();
		Map<String, String> result = new HashMap<String, String>();
		result.put("score", new Integer(score).toString());
		result.put("scorePct", new Float(scorePct).toString());
		mav.setViewName("result");
		mav.addObject("result", result);

		return mav;
	}

}
