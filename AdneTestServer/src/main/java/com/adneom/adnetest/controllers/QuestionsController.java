package com.adneom.adnetest.controllers;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adneom.adnetest.Category;
import com.adneom.adnetest.Level;
import com.adneom.adnetest.Question;
import com.adneom.adnetest.DAO.IQuestionsDAO;
import com.adneom.adnetest.DAO.QuestionsDAOImpl;

@Controller
public class QuestionsController {

	private static final Logger logger = LoggerFactory
			.getLogger(QuestionsController.class);
	private IQuestionsDAO dao = new QuestionsDAOImpl();

	@RequestMapping(value = "/categories")
	public @ResponseBody List<Category> categories(Locale locale, Model model) {

		return dao.getCategories();

	}

	@RequestMapping(value = "/levels")
	@ResponseBody
	public List<Level> levels(Locale locale, Model model,
			@RequestParam(value = "catid") String catid) {

		Category cat = new Category();
		cat.setId(catid);
		return dao.getLevels(cat);
	}
	
	@RequestMapping(value = "/questions")
	@ResponseBody
	public List<Question> levels(Locale locale, Model model,
			@RequestParam(value = "catid") String catid,
			@RequestParam(value = "levelid") String levelId) {

		Category cat = new Category();
		cat.setId(catid);
		Level level = new Level();
		level.setId(levelId);
		return dao.getQuestions(cat, level);
	}

}
