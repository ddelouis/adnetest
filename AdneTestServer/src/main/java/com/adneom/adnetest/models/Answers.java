package com.adneom.adnetest.models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adneom.adnetest.Category;
import com.adneom.adnetest.Level;
import com.adneom.adnetest.Question;

public class Answers {

	private static final Logger logger = LoggerFactory.getLogger(Answers.class);

	List<Answer> answers = new ArrayList<Answer>();

	public List<Answer> getAnswers() {
		return answers;
	}

	public void fromJSON(String json) {
		ObjectMapper m = new ObjectMapper();
		ArrayNode rootNode = null;
		try {
			rootNode = (ArrayNode) m.readTree(json);
		} catch (JsonProcessingException e) {
			logger.error("Error when loading the JSON containig the answers", e);
		} catch (IOException e) {
			logger.error("Error when loading the JSON containig the answers", e);
		}

		if (rootNode != null) {
			for (JsonNode arrayElem : rootNode) {
				JsonNode category = arrayElem.get("category");

				Answer ans = new Answer();
				
				ans.getCat().setId(category.get("idCat").asText());

				ans.getLevel().setId(category.get("idNiveau").asText());

				ans.getQuestion().setTitle(arrayElem.get("question").get("title")
						.asText());

				ans.setIndex(arrayElem.get("indexAnswer").asInt());

				answers.add(ans);
			}
		}
	}
}
