package com.adneom.adnetest.models;

import com.adneom.adnetest.Category;
import com.adneom.adnetest.Level;
import com.adneom.adnetest.Question;

public class Answer {

	private int index;
	private Category cat  = new Category();
	private Level level = new Level();
	private Question question  = new Question();
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int i) {
		index = i;
	}

	public Category getCat() {
		return cat;
	}

	public Level getLevel() {
		return level;
	}

	public Question getQuestion() {
		return question;
	}

}
