package com.adneom.adnetest.DAO;

import java.util.List;

import com.adneom.adnetest.Category;
import com.adneom.adnetest.Level;
import com.adneom.adnetest.Question;

public interface IQuestionsDAO {

	public List<Category> getCategories();
	public List<Level> getLevels(Category cat);
	public List<Question>getQuestions(Category cat, Level level);
	
}
