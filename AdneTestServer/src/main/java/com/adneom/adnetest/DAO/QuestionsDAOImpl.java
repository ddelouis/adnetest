package com.adneom.adnetest.DAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.adneom.adnetest.Categories;
import com.adneom.adnetest.Category;
import com.adneom.adnetest.Level;
import com.adneom.adnetest.Question;
import com.adneom.adnetest.controllers.HomeController;

public class QuestionsDAOImpl implements IQuestionsDAO {

	private static final Logger logger = LoggerFactory
			.getLogger(QuestionsDAOImpl.class);

	private JAXBContext context;
	Categories categories;

	public QuestionsDAOImpl() {
		Unmarshaller unmarshaller = null;
		try {
			context = JAXBContext.newInstance("com.adneom.adnetest");

			unmarshaller = context.createUnmarshaller();
			if (unmarshaller != null) {
				categories = (Categories) unmarshaller
						.unmarshal(new ClassPathResource("qcm.xml").getFile());
			}

		} catch (JAXBException e) {
			logger.error("Error when instantiating the DAO", e);

		} catch (IOException e) {
			logger.error("Error when instantiating the DAO", e);
		}

	}

	@Override
	public List<Category> getCategories() {

		return categories.getCategory();
	}

	@Override
	public List<Level> getLevels(Category cat) {

		for (Category category : categories.getCategory()) {
			if (category.getId().equals(cat.getId())) {
				return category.getLevel();
			}
		}

		return new ArrayList<Level>();
	}

	@Override
	public List<Question> getQuestions(Category cat, Level lvl) {

		for (Level level : getLevels(cat)) {
			if (level.getId().equals(lvl.getId())) {
				return level.getQuestions().get(0).getQuestion();
			}
		}

		return null;
	}

}
